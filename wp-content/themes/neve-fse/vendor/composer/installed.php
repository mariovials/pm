<?php return array(
    'root' => array(
        'name' => 'codeinwp/neve-fse',
        'pretty_version' => 'v1.0.4',
        'version' => '1.0.4.0',
        'reference' => 'f01d8627a32f5b8ddd8d1ef05835d8070448dcde',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'codeinwp/neve-fse' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'reference' => 'f01d8627a32f5b8ddd8d1ef05835d8070448dcde',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'codeinwp/themeisle-sdk' => array(
            'pretty_version' => '3.3.1',
            'version' => '3.3.1.0',
            'reference' => 'efb66935e69935b21ad99b0e55484e611ce4549d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeinwp/themeisle-sdk',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
