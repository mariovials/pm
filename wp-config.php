<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pm' );

/** MySQL database username */
define( 'DB_USER', 'planmaestro' );

/** MySQL database password */
define( 'DB_PASSWORD', '3@e15m^6@iMg' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_?(!929l4/:=jw:G(?hk-GBG,;aqSV^F21j/4{,Gu~8oVPLyPS|Y_PRI{C[#y|=h' );
define( 'SECURE_AUTH_KEY',  'Kq8xmV[DD+}`|lPAbSgsh,aj-=<=rVD||o?}v;g #<1yw&]m&~ cWw$,~AAaf1S8' );
define( 'LOGGED_IN_KEY',    '$C(^1BnV6zqDg}e9t S8*.joxvIw=z)!g(Q|KK6g6!qIW[6[prJ$c-Z?a|zLp[}d' );
define( 'NONCE_KEY',        'TDs=2jQt.NLXmaQ^,p>9u+:&dw]K#&;-%=Jm <2qQ[*n4DUd69N$?i0G{L2Deg3p' );
define( 'AUTH_SALT',        'gzzy>3~HA$8MUBRjYCYs:xSd`7yZ|N@wRE6VqT611>:otw3`3 Kx,L1}=ONWokUS' );
define( 'SECURE_AUTH_SALT', '`=|#sE-_]4*HC%C1m7h|M$iCS:R&-Hh a>R+G?_MaAkg7U60josyQ!XCw6HZ^ktT' );
define( 'LOGGED_IN_SALT',   'U<5GdRY[<3LG;Udk.yW8*o3A9tYbZ_,#=!+&0Q;JF0d,#@}C c#-M&(H5m :1>1x' );
define( 'NONCE_SALT',       '26uS[%ux_4`cpl!)_p99%esu{27ziT3>;Dyi{=1T:xdwg>Q{NuP9sg;toXVvrK0)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
  define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
